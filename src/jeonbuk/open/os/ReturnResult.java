package jeonbuk.open.os;
/**
	 * @author jwh0728 결과 처리용
	 */
	public class ReturnResult {
		public ReturnResult(boolean success, String message, String data) {
			super();
			this.success = success;
			this.message = message;
			this.data = data;
		}
		public ReturnResult(boolean success, String message) {
			super();
			this.success = success;
			this.message = message;
			this.data = "";
		}
		private String data;
		public String getData() {
			return data;
		}
		public void setData(String data) {
			this.data = data;
		}
		private boolean success = false;
		public boolean isSuccess() {
			return success;
		}
		public void setSuccess(boolean success) {
			this.success = success;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		private String message = "";

		public String toJSONString() {
			return "{ \"success\" : " + (success ? "true" : "false") + ", \"message\" : \"" + message + "\" "
					+ ", \"data\" : \"" + data + "\" }";
		}
	}
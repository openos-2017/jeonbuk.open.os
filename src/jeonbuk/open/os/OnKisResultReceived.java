package jeonbuk.open.os;

import org.json.JSONException;

public interface OnKisResultReceived {
	void returnReceivedData(KisResultOptions resultOptions) throws JSONException;

	
}

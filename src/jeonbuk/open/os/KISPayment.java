package jeonbuk.open.os;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;

import tw.com.prolific.driver.pl2303.PL2303Driver;
import android.content.Context;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.util.Log;


public class KISPayment extends KISCommon{
	
	private Context context;
	
	private static final byte[] DLE_5 = new byte[]{DLE, DLE, DLE, DLE, DLE}; 
	private static final byte[] ACK_RESPONSE = new byte[]{ACK, ACK, ACK, ACK, ACK};
	private static final byte[] NAK_RESPONSE = new byte[]{NAK};

	// serial 
	private PL2303Driver serial;
	public PL2303Driver getSerial() {
		return serial;
	}

	public void setSerial(PL2303Driver serial) {
		this.serial = serial;
	}

	private static final String ACTION_USB_PERMISSION = "com.prolific.pl2303hxdsimpletest.USB_PERMISSION";

	/**
	 * 접속 체크시 접속 완료  
	 */
	public static final int DEVICE_CONNECTED = 0;

	/**
	 * 접속 오류 : 권한 없음 
	 */
	public static final int CANNOT_OPEN_NO_PERMISSION = 1;

	/**
	 * 접속 오류 : 장비의 칩이 다름 
	 */
	public static final int CANNOT_OPEN_CHIP_NOT_SUPPORT = 2;

	/**
	 * 전송됨 
	 */
	public static final int COMMAND_SEND_OK = 3;

	/**
	 * 전송 후 응답 대기 상태 (카드결제의 경우 단말기에 '카드를 읽혀주세요' 상태)   
	 */
	public static final int ACK_READ = 4;

	/**
	 * 전송 후 결과 받음 
	 */
	public static final int RESULT_RECEIVED = 5;

	/**
	 * 1차 재전송 시도 
	 */
	public static final int RETRY_FIRST_SEND = 6;

	/**
	 * 1차 재전송 후 결과 반환 
	 */
	public static final int RETRY_FIRST_RESULT_RECEIVED = 7;

	/**
	 * 2차 재전송 시도 
	 */
	public static final int RETRY_SECOND_SEND = 8;

	/**
	 * 2차 재전송 결과 반환 
	 */
	public static final int RETRY_SECOND_RESULT_RECEIVED = 9;

	/**
	 * 2차 전송 후 최종 에러 
	 */
	public static final int RETRY_SECOND_ERROR = 10;

	/**
	 * 전문 오류 
	 */
	public static final int COMMAND_ERROR = 11;
	
	private PL2303Driver.BaudRate mBaudrate = PL2303Driver.BaudRate.B115200;
	private PL2303Driver.DataBits mDataBits = PL2303Driver.DataBits.D8;
	private PL2303Driver.Parity mParity = PL2303Driver.Parity.NONE;
	private PL2303Driver.StopBits mStopBits = PL2303Driver.StopBits.S1;
	private PL2303Driver.FlowControl mFlowControl = PL2303Driver.FlowControl.OFF;
	
	private OnKisResultReceived onResultReceived;
	
	private Thread communicationThread = null;
	
	/**
	 * 메시지 핸들러
	 */
	private Handler handler = new Handler();
	
	public Handler getHandler() {
		return handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}
	
	/**
	 * print image path
	 * "" 이면 출력하지 않음 
	 */
	private String logoImagePath = "";
	
	public String getLogoImagePath() {
		return logoImagePath;
	}

	public void setLogoImagePath(String logoImagePath) {
		this.logoImagePath = logoImagePath;
	}
	
	

	/**
	 * 생성자 
	 */
	public KISPayment(final Context context) {
		super();
		this.context = context;
		
	
		serial = new PL2303Driver((UsbManager) context.getSystemService(Context.USB_SERVICE), context, ACTION_USB_PERMISSION);
		
	//	System.out.println("serial===>"+serial.);
		
		// check USB host function.
		if (!serial.PL2303USBFeatureSupported()) {
			Log.d("onCreate", "No Support USB host API");
			serial = null;
			return;
		}
		
	}
	
	/**
	 * 현금영수증 취소 
//		KisOptions options = new KisOptions();
//		options.setServiceCode(KISPayment.TAX_POINT_CANCEL);
//		options.setPaymentType(KISPayment.PAYMENT_TYPE_CASH);
//		options.setPaymentAmount("1000"); // 금액 
//		options.setApprovalCard("01073020728"); // 전화번호 등 ..
//		options.setApprovalPaymonth("0"); // 할부 개월 수
//		options.setApprovalDate("2016-08-17"); // 승인 일자 
//		options.setApprovalNo("090076536"); // 승인 번호 
	 */
	public void cashPaymentCancel(KisOptions options) {
		
		options = getVanCancelOption(options);
		cardPayment(options);
	}

	/**
	 * 현금영수증 결제 
//		KisOptions options = new KisOptions();
//		options.setPaymentAmount("1000"); // 금액 
//		options.setServiceCode(KISPayment.TAX_POINT_APPROVAL);
//		options.setApprovalCard("01073020728"); // 전화번호 등 .. 
//		options.setApprovalPaymonth(KISPayment.CASH_RECEIPT_TYPE_PRIVATE);  // 01:법인, 02:개인
	 */
	public void cashPayment(KisOptions options) {
		cardPayment(options);
	}

	/**
	 * 카드 결제 취소 
//		KisOptions options = new KisOptions();
//		options.setServiceCode(KISPayment.CREDIT_CANCEL);
//		options.setPaymentType(KISPayment.PAYMENT_TYPE_CARD);
//		options.setPaymentAmount("1000"); // 금액 
//		options.setApprovalPaymonth("0"); // 할부 개월 수
//		options.setApprovalDate("2016-08-17"); // 승인 일자 
//		options.setApprovalNo("62086245"); // 승인 번호 
	 */
	public void cardPaymentCancel(KisOptions options) {
		options = getVanCancelOption(options);
		cardPayment(options);
	}

	
	/** 카드결제 
	 * @param options
	 */
	public void cardPayment(final KisOptions options){
		if(communicationThread != null){
			communicationThread.interrupt();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				System.out.println("에러");
				//e.printStackTrace();
			}
			if(communicationThread.isInterrupted()){
				communicationThread = null;
			}
		}
		communicationThread = new Thread(new Runnable() {

			@Override
			public void run() {
				openUsbSerial();
				
				JSONArray jsonArray = createCommonRequest(options);
				byte[] command;
				ReturnResult result;
				try {
					command = jsonArrayToByteArray(jsonArray);
					result = credit(command);
					Log.d("cardPayment", result.toJSONString());
					if(result.isSuccess()){
						KisResultOptions resultOptions = parseCommonResponse(result.toJSONString());
						onResultReceived.returnReceivedData(resultOptions);
					}else{
						onResultReceived.returnReceivedData(null);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		communicationThread.start();
	}
	
	/** 프린팅  
	 * @param message
	 */
	public void printMessage(final String message, final String parameter){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				openUsbSerial();
				
				ReturnResult result;
				//byte[] command = { 0x1d, 0x56, 0x01}; //cutting
				byte[] command = {0x1b, 0x69};
				try {
					result = init(message, command);
					Log.d("printMessage", result.toJSONString());
					
					if(result.isSuccess()){
						KisResultOptions resultOptions = new KisResultOptions();
						resultOptions.setParameter(parameter);
						onResultReceived.returnReceivedData(resultOptions);
					}else{
						onResultReceived.returnReceivedData(null);
					}
				} catch (Exception e) {
					
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	/** 메세지 출력 후에 바코드를 찍고(숫자 또는 영문자) 컷팅한다. 
	 * @param message
	 * @param barcode
	 * @param parameter(결과 구분용)
	 */
	public void printMessageAndBarcode(final String message, final String barcode, final String parameter){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				openUsbSerial();
				
				ReturnResult result;
				//byte[] command = { 0x1d, 0x56, 0x01}; //cutting     (byte)0x70 .(byte)0xA0 ,0x30
				//byte[] command = {0x1B, 0x40, 0x0A, 0x1B, 0x61, 0x01, 0x1D, 0x6B, 0x49, 0x10, 0x5A, 0x30, 0x30, 0x33, 0x32, 0x30, 0x30, 0x36, 0x30, 0x33, 0x30, 0x38, 0x30, 0x30, 0x30, 0x31, 0x0A, 0x1B, 0x40, 0x0A, 0x1B, 0x69};
				//byte[] precommand = {0x1B, 0x40, 0x0A, 0x1B, 0x61, 0x01, 0x1D, 0x6B, 0x49};
				byte[] precommand = {/* init */ 0x1B, 0x40, 
									 /* line feed */ 0x0A,  
									 /* barcode center */ 0x1B, 0x61, 0x01,  
									 /* barcode height */ 0x1d, 0x68,(byte)0xC0 , 
									 /* barcode width */ 0x1d, 0x77, 0x03,
									 /* barcode print */ 0x1D, 0x6B, 0x49};
				byte[] aftercommand = {/* line feed */ 0x0A, 
										/* init */ 0x1B, 0x40, 
										/* line feed */ 0x0A, 
										/* paper cut */ 0x1B, 0x69};
				
				ByteArrayBuffer buffer = new ByteArrayBuffer(2000);
				
				byte[] barcodeByte;
				try {
					barcodeByte = barcode.getBytes("EUC-KR");
					byte[] barcodeByteLength = {(byte)(barcodeByte.length)};
					buffer.append(precommand, 0, precommand.length);
					buffer.append(barcodeByteLength, 0, 1);
					buffer.append(barcodeByte, 0, barcodeByte.length);
					buffer.append(aftercommand, 0, aftercommand.length);
					
					result = init(message, buffer.buffer());
					Log.d("printMessage", result.toJSONString());

					if(result.isSuccess()){
						KisResultOptions resultOptions = new KisResultOptions();
						resultOptions.setParameter(parameter);
						onResultReceived.returnReceivedData(resultOptions);
					}else{
						onResultReceived.returnReceivedData(null);
					}
				} catch (Exception e) {
					System.out.println("에러");
					//e.printStackTrace();
				}
			}
		}).start();
	}
	
	/**
	 * 결제 과정을 중단하고 초기화면으로 돌아간다. 
	 */
	public void stopAndInitPayment(){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// stop old communication thread 
				if(communicationThread != null){
					communicationThread.interrupt();
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						System.out.println("에러");
						//e.printStackTrace();
					}
					if(communicationThread.isInterrupted()){
						communicationThread = null;
					}
				}
				
				openUsbSerial();
				
				ReturnResult result;
				byte[] command = {0x10, 0x10, 0x10, 0x10, 0x10};
				try {
					result = init("", command);
					Log.d("printMessage", result.toJSONString());
					
					if(result.isSuccess()){
						KisResultOptions resultOptions = new KisResultOptions();
						onResultReceived.returnReceivedData(resultOptions);
					}else{
						onResultReceived.returnReceivedData(null);
					}
				} catch (Exception e) {
					System.out.println("에러");
				//	e.printStackTrace();
				}
			}
		}).start();
	}
	
	/**
	 * 결제를 시도한다.
	 * 
	 * @param command
	 * @return
	 * @throws Exception
	 */
	private ReturnResult credit(byte[] command) throws Exception {

		int res = serial.write(command, command.length);
		if( res<0 ) {
			Log.d("init", "setup2: fail to controlTransfer: "+ res);
			return new ReturnResult(false, "error");
		} 
		
		Log.d("SocketPlugin", "command send ok");
		handler.sendEmptyMessage(COMMAND_SEND_OK);
		
		// delay..
		//Thread.sleep(1000);
		
		byte[] data = new byte[512];
		int ret;
		boolean success = false;
		boolean isIcPosSyn = false;
		
		do{
			while((ret = serial.read(data)) == 0){
				Thread.sleep(100);
			}
			// IC-POS 연동 상태에서 IC 입력이면 다시 ACK를 기다림 
			for (int i = 0; i < ret; i++) {
				if (data[i] == ACK) {
					success = true;
					isIcPosSyn = false;
					break;
				}
				if(data[i] == SYN){
					success = false;
					isIcPosSyn = true;
					break;
				}
			}
		}while(isIcPosSyn || !success);
		
		Log.d("SocketPlugin", "첫번째 응답 읽음 (" + (success == true ? "ACK이다" : "ACK아니다") + ") " + bytesToHex(data));

		StringBuffer result = new StringBuffer();
		if (success) {
			handler.sendEmptyMessage(ACK_READ);
			ret = 0;
			int totalLength = 0;
			while((ret = serial.read(data)) == 0){
				Thread.sleep(50);
			}
			do{
				for (int i = 0; i < ret; i++) {
					if (result.length() > 0) {
						result.append(",");
					}
					result.append(0xff & data[i]);
				}
				totalLength += ret;

				Thread.sleep(50);
			}while((ret = serial.read(data)) > 0);
				
			Log.d("SocketPlugin", "결제내역 전송 받음 : " + totalLength);
			handler.sendEmptyMessage(RESULT_RECEIVED);
			
			try{
				// 응답이 2로 시작하는지 검사 
				if(!result.toString().split(",")[0].equals("2")){
					throw new Exception("first character error");
				}
				// 전문 검사 
				if(parseCommonResponse(new ReturnResult(true, "ok", result.toString()).toJSONString()) == null){
					throw new Exception("parse error");
				}
			}catch(Exception e){
				// 1차 재시도 
			//	e.printStackTrace();
				Log.d("Exception", "NAK전송 : 1차 재시도 ");
				handler.sendEmptyMessage(RETRY_FIRST_SEND);
				
				serial.write(NAK_RESPONSE, NAK_RESPONSE.length);
				
				ret = 0;
				totalLength = 0;
				result = new StringBuffer();
				
				while((ret = serial.read(data)) == 0){
					Thread.sleep(50);
				}
				do{
					for (int i = 0; i < ret; i++) {
						if (result.length() > 0) {
							result.append(",");
						}
						result.append(0xff & data[i]);
					}
					totalLength += ret;

					Thread.sleep(50);
				}while((ret = serial.read(data)) > 0);
				Log.d("SocketPlugin", "결제내역 전송 받음 : " + totalLength);
				handler.sendEmptyMessage(RETRY_FIRST_RESULT_RECEIVED);
				
				try{
					// 응답이 2로 시작하는지 검사 
					if(!result.toString().split(",")[0].equals("2")){
						throw new Exception("first character error");
					}
					// 전문 검사 
					if(parseCommonResponse(new ReturnResult(true, "ok", result.toString()).toJSONString()) == null){
						throw new Exception("parse error");
					}
				}catch(Exception e1){
					// 2차 재시도 
					e1.printStackTrace();
					Log.d("Exception", "NAK전송 : 2차 재시도 ");
					handler.sendEmptyMessage(RETRY_SECOND_SEND);
					
					serial.write(NAK_RESPONSE, NAK_RESPONSE.length);
					
					ret = 0;
					totalLength = 0;
					result = new StringBuffer();
					
					while((ret = serial.read(data)) == 0){
						Thread.sleep(50);
					}
					do{
						for (int i = 0; i < ret; i++) {
							if (result.length() > 0) {
								result.append(",");
							}
							result.append(0xff & data[i]);
						}
						totalLength += ret;
//						if(totalLength > 100){
//							break;
//						}
						Thread.sleep(50);
					}while((ret = serial.read(data)) > 0);
					Log.d("SocketPlugin", "결제내역 전송 받음 : " + totalLength);
					handler.sendEmptyMessage(RETRY_SECOND_RESULT_RECEIVED);
					
					try{
						// 응답이 2로 시작하는지 검사 
						if(!result.toString().split(",")[0].equals("2")){
							throw new Exception("first character error");
						}
						// 전문 검사 
						if(parseCommonResponse(new ReturnResult(true, "ok", result.toString()).toJSONString()) == null){
							throw new Exception("parse error");
						}
					}catch(Exception e2){
						e2.printStackTrace();
						Log.e("Exception", "2차 재시도 오류 ");
						Log.d("SocketPlugin", result.toString());
						handler.sendEmptyMessage(RETRY_SECOND_ERROR);
						return new ReturnResult(false, "통신 오류");
					}
				}
			}

			serial.write(ACK_RESPONSE, ACK_RESPONSE.length);

			Log.d("SocketPlugin", result.toString());
			return new ReturnResult(true, "ok", result.toString());
		} else {
			serial.write(DLE_5, DLE_5.length);
			Log.d("SocketPlugin", "command error");
			handler.sendEmptyMessage(COMMAND_ERROR);
			return new ReturnResult(false, "command error");
		}

	}
	
	/** init을 통해 printmessage등을 보낸다. 
	 * @param host
	 * @param port
	 * @param message
	 * @param command
	 * @param timeout
	 * @return
	 * @throws Exception
	 */
	private ReturnResult init(String message, byte[] command) throws Exception {
		try {
			int res;
			if(!logoImagePath.equals("")){
				byte[] imageCommand = getPrintImageCommand(logoImagePath);
				if(imageCommand != null){
					res = serial.write(imageCommand, imageCommand.length);
					if( res<0 ) {
						Log.d("init", "setup2: fail to controlTransfer: "+ res);
						return new ReturnResult(false, "error");
					}
				}
			}
			if(message.length() > 0){
				res = serial.write(message.getBytes("EUC-KR"), message.getBytes("EUC-KR").length);
				if( res<0 ) {
					Log.d("init", "setup2: fail to controlTransfer: "+ res);
					return new ReturnResult(false, "error");
				} 
			}
			res = serial.write(command, command.length);
			if( res<0 ) {
				Log.d("init", "setup2: fail to controlTransfer: "+ res);
				return new ReturnResult(false, "error");
			}
			Thread.sleep(300);

			return new ReturnResult(true, "ok");
		} catch (Exception e) {
			Log.d("TCPSockets", "Exception");
			return new ReturnResult(false, "Exception");
		} 
	}
	
	/**
	 * serial open 
	 */
	private void openUsbSerial() {
		try {
			if(null==serial)
				return;   	 
	
			if (serial.isConnected()) {
	//			mBaudrate = PL2303Driver.BaudRate.B115200;
				// if (!mSerial.InitByBaudRate(mBaudrate)) {
					if (!serial.InitByBaudRate(mBaudrate,700)) {
						if(!serial.PL2303Device_IsHasPermission()) {
							Log.e("openUsbSerial", "cannot open, maybe no permission");
							handler.sendEmptyMessage(CANNOT_OPEN_NO_PERMISSION);
						}
	
						if(serial.PL2303Device_IsHasPermission() && (!serial.PL2303Device_IsSupportChip())) {
							Log.e("openUsbSerial", "cannot open, maybe this chip has no support, please use PL2303HXD / RA / EA chip.");
							handler.sendEmptyMessage(CANNOT_OPEN_CHIP_NOT_SUPPORT);
						}
					} else {        	
						Log.e("openUsbSerial", "connected : " );
						handler.sendEmptyMessage(DEVICE_CONNECTED);
					}
			}//isConnected

			Thread.sleep(1000);
			serial.setup(mBaudrate, mDataBits, mStopBits, mParity, mFlowControl);
		} catch (Exception e) {
			System.out.println("에러");
		//	e.printStackTrace();
		} 
		
	}//openUsbSerial
	
	/** jsonArray to byte array 
	 * @param arr
	 * @return
	 * @throws JSONException
	 */
	private byte[] jsonArrayToByteArray(JSONArray arr) throws JSONException {
		byte[] byteArr = new byte[arr.length()];
		for (int i = 0; i < arr.length(); i++) {
			byteArr[i] = (byte) arr.getInt(i);
		}
		return byteArr;
	}
	
	
	/** 취소 관련 옵션 설정 
	 * @param options
	 * @return
	 */
	private KisOptions getVanCancelOption(KisOptions options){
		options.setApprovalDate(options.getApprovalDate().replace("-", ""));
		if(options.getApprovalDate().length() == 8){
			options.setApprovalDate(options.getApprovalDate().substring(2, 8));
		}
		
		if(options.getPaymentType().equals(PAYMENT_TYPE_CASH)){
			options.setWcc(WCC_KEYIN);
			options.setServiceCode(TAX_POINT_CANCEL);
			options.setMaskMessage("*현금영수증취소*<br/><br/>카드단말기의 입력 버튼을 눌러 주십시오.");
		}else if(options.getPaymentType().equals(PAYMENT_TYPE_CARD)){
			options.setWcc(WCC_SWIPE);
			options.setServiceCode(CREDIT_CANCEL);
			options.setMaskMessage("*신용카드취소*<br/><br/>카드를 읽어 주세요.");
		}
		
		return options;
	}

	/** 결과 수신 메소드 
	 * @return
	 */
	public OnKisResultReceived getOnResultReceived() {
		return onResultReceived;
	}

	/** 결과 수신 메소드 
	 * @param onResultReceived
	 */
	public void setOnResultReceived(OnKisResultReceived onResultReceived) {
		this.onResultReceived = onResultReceived;
	}
	
	/**
	 * 돈통을 연다 
	 */
	public void openCachBox(final String parameter){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				openUsbSerial();
				
				ReturnResult result;
				// 1B;40;1B;70;00;00;00
				byte[] command = {0x1b, 0x40, 0x1b, 0x70, 0x00, 0x00, 0x00};
				try {
					result = init("", command);
					Log.d("printMessage", result.toJSONString());
					
					if(result.isSuccess()){
						KisResultOptions resultOptions = new KisResultOptions();
						resultOptions.setParameter(parameter);
						onResultReceived.returnReceivedData(resultOptions);
					}else{
						onResultReceived.returnReceivedData(null);
					}
				} catch (Exception e) {
					System.out.println("에러");
				//	e.printStackTrace();
				}
			}
		}).start();
	}
	
	/** monochrome 이미지를 인쇄한다.
	 * (288 x YYY) bmp image를 기준으로 함 (YYY < 288) 
	 * @param imgPath
	 * @param parameter
	 */
	public void printImage(final String imgPath, final String parameter){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				openUsbSerial();
				
				ReturnResult result;
				
				byte[] command = getPrintImageCommand(imgPath);
				
				try {
					result = init("", command);
					Log.d("printMessage", result.toJSONString());
					
					if(result.isSuccess()){
						KisResultOptions resultOptions = new KisResultOptions();
						resultOptions.setParameter(parameter);
						onResultReceived.returnReceivedData(resultOptions);
					}else{
						onResultReceived.returnReceivedData(null);
					}
				} catch (Exception e) {
					System.out.println("에러");
				//	e.printStackTrace();
				}
			}
		}).start();
	}
	
	/** monochrome 이미지 data return 
	 * (288 x YYY) bmp image를 기준으로 함 (YYY < 288) 
	 * @param imgPath
	 * @param parameter
	 * 오류시 null 반환 
	 */
	public byte[] getPrintImageCommand(String imgPath){
		ImageData imageData = getBitmapData(imgPath);
		if(imageData == null) return null;
		
		ByteArrayBuffer buffer = new ByteArrayBuffer(imageData.getImgData().length + 2000);

		byte[] command = {/* init */ 0x1B, 0x40, 
				/* set image info */ 0x1d, 0x76, 0x30, 0x03, 
							(byte)((imageData.getWidth() / 8) % 256), 
							(byte)((imageData.getWidth()/8) / 256), 
							(byte)(imageData.getHeight() % 256), 
							(byte)(imageData.getHeight() / 256)
		};
		
		buffer.append(command, 0, command.length);
		buffer.append(imageData.getImgData(), 0, imageData.getImgData().length);
		return buffer.buffer();
	}
	
	
	/**
	 * monochrome 이미지 데이터를 인쇄용에 맞게 가져온다. 
	 * https://en.wikipedia.org/wiki/BMP_file_format
	 * @return
	 */
	private ImageData getBitmapData(String imagePath){
		try {
			File file = new File(imagePath);
			byte[] fileData = new byte[(int) file.length()];
			DataInputStream dis = new DataInputStream(new FileInputStream(file));
			dis.readFully(fileData);
			dis.close();
			
			// get pixel data offset 
			int offset = fileData[(int)0x0a];
			// pixel data length 
			int dataLength = fileData.length - offset;
			// get pixel data 
			byte[] dstData = new byte[dataLength];
			System.out.println(dataLength);
			System.arraycopy(fileData, offset, dstData, 0, dataLength);
			
			// 역상 처리 (Bit BMP -> 인쇄용)
			for(int i = dataLength - 1; i >= 0; i--){
				dstData[i] = (byte)(dstData[i] ^ 0xFF);
			}
			
			// get width, height 
			int width = (fileData[0x12] & 0xff) | ((fileData[0x13] & 0xff) << 8) | ((fileData[0x14] & 0xff) << 16) | ((fileData[0x15] & 0xff) << 24);
			int height = (fileData[0x16] & 0xff) | ((fileData[0x17] & 0xff) << 8) | ((fileData[0x18] & 0xff) << 16) | ((fileData[0x19] & 0xff) << 24);
			System.out.println("width : " + width + " height : " + height );
			
			// 상하 바꾸기(인쇄용 고려)
			byte[] flip = new byte[dataLength];
			int fOffset = 0;
			for(int j = 0; j < height; j++ ){
				System.arraycopy(dstData, fOffset, flip, (width / 8) * ((height - 1) - j), width / 8);
				fOffset += (width / 8);
			}
			
			return new ImageData(width, height, flip);
		} catch (IOException e) {
			System.out.println("에러");
			//	e.printStackTrace();
		}
		return null;
	}
	
	public class ImageData{
		public ImageData(int width, int height, byte[] imgData) {
			super();
			this.width = width;
			this.height = height;
			this.imgData = imgData;
		}
		private int width = 0; 
		private int height = 0;
		private byte [] imgData = null;
		public int getWidth() {
			return width;
		}
		public void setWidth(int width) {
			this.width = width;
		}
		public int getHeight() {
			return height;
		}
		public void setHeight(int height) {
			this.height = height;
		}
		public byte[] getImgData() {
			return imgData;
		}
		public void setImgData(byte[] imgData) {
			this.imgData = imgData;
		}
		
	}
	
}

package jeonbuk.open.os;

import org.json.JSONException;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	Activity act;
	private KISPayment kisPayment; 
	
	TextView tv_test;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		 act =this;
		
		act.findViewById(R.id.btn_test_0).setOnClickListener(onClickListener);
		act.findViewById(R.id.btn_test_1).setOnClickListener(onClickListener);
		act.findViewById(R.id.btn_test_2).setOnClickListener(onClickListener);
		act.findViewById(R.id.btn_test_3).setOnClickListener(onClickListener);
		act.findViewById(R.id.btn_test_4).setOnClickListener(onClickListener);
		
		
		 kisPayment = new KISPayment(getApplicationContext());
		 KisHandler thread = new KisHandler();
		 kisPayment.setHandler(thread);
		
		 tv_test  =(TextView)act.findViewById(R.id.tv_test);
		 
		 PaymentProcess();
	}

	private class KisHandler extends Handler{
		@Override
		public void handleMessage(Message msg) {
			
		 switch(msg.what){
		 	case KISPayment.DEVICE_CONNECTED :

		    Log.d("handleMessage", "장비 접속");
		   
		  	break;
			case KISPayment.COMMAND_SEND_OK :

			    Log.d("handleMessage", "전송 완료");
			  
			  	break;
			case KISPayment.ACK_READ :

			    Log.d("handleMessage", "카드를 읽혀주세요");
			    
			  	break;
			case KISPayment.RESULT_RECEIVED :

			    Log.d("handleMessage", "전송후 결과을 받고 있습니다");
			  
			  	break;
			case KISPayment.RETRY_FIRST_SEND :

			    Log.d("handleMessage", "1차 재전송 시도");
			   
			  	break;
			case KISPayment.RETRY_FIRST_RESULT_RECEIVED :

			    Log.d("handleMessage", " 1차 재전송 후 결과 반환");
			   
			  	break;
			case KISPayment.RETRY_SECOND_SEND :

			    Log.d("handleMessage", "2차 재전송 시도 ");
			  
			  	break;
			case KISPayment.RETRY_SECOND_RESULT_RECEIVED :

			    Log.d("handleMessage", " 2차 재전송 결과 반환  ");
			   
			  	break;
		  
		  
		  
		 }
		 super.handleMessage(msg);
		}

	}
	
	
	 Button.OnClickListener onClickListener = new Button.OnClickListener(){
			public void onClick(View v) {
				
				switch(v.getId()){
				
				case R.id.btn_test_0:
					tv_test.setText("");
					card();
					
				break;
				case R.id.btn_test_1:
					tv_test.setText("");
					cardReturn();
				break;
				case R.id.btn_test_2:
					tv_test.setText("");
					cashPayment();
				break;
				case R.id.btn_test_3:
					tv_test.setText("");
					print();
					
				break;
				case R.id.btn_test_4:
					tv_test.setText("");
					cashBox_open();
					
				break;
			
				
				}
			
			}
	 
	 	};
	 	
	 	
	 	/*
	 	 * 카드결제
	 	 * */
	 	public void card() {
	 		
	 		KisOptions options = new KisOptions();
			options.setPaymentAmount("1000"); // 금액
			options.setServiceCode(KISPayment.CREDIT_APPROVAL);  //TAX_POINT_APPROVAL  현금  ,CREDIT_APPROVAL 카드
			options.setApprovalPaymonth("0"); // 할부 개월 수 
			kisPayment.cardPayment(options);
	 		
	 	}
	 	
		/*
	 	 * 카드결제 환불
	 	 * */
	 	public void cardReturn() {
	 		
		 
		KisOptions options = new KisOptions();
		options.setServiceCode(KISPayment.CREDIT_CANCEL);
		options.setPaymentType(KISPayment.PAYMENT_TYPE_CARD);
		options.setPaymentAmount("1000"); // 금액 
		options.setApprovalPaymonth("0"); // 할부 개월 수
		options.setApprovalDate("20171011"); // 승인 p일자 
		options.setApprovalNo("1111111"); // 승인 번호 
		kisPayment.cardPayment(options);
		 
	 		
	 	}
	 	
	 	
		/*
	 	 * 현금영수증
	 	 * */
	 	public void cashPayment() {
	 		
	 		
	 		KisOptions options = new KisOptions();
			options.setPaymentAmount("1000");
			options.setServiceCode(KISPayment.TAX_POINT_APPROVAL);
			options.setApprovalCard("01091125737"); // 전화번호 등 .. 
			options.setApprovalPaymonth(KISPayment.CASH_RECEIPT_TYPE_PRIVATE);  // 01:법인, 02:개인
			kisPayment.cardPayment(options);
	 	}
		/*
	 	 * 프린터
	 	 * */
	 	public void print() {
	 		kisPayment.setLogoImagePath("");
	 		kisPayment.printMessage("testtesttesttesttesttesttesttesttesttest\r\ntesttesttesttesttesttesttest\r\n\r\n\r\n\r\n","1");
	 		
	 	}
	 	
		/*
	 	 * 돈통
	 	 * */
	 	public void cashBox_open() {
	 		kisPayment.setLogoImagePath("");
			kisPayment.openCachBox("A");
	 	}
	 	
	 	
	 	
	 	
	 	
	 	@Override
		public void onResume() {
			Log.d("onResume", "Enter onResume"); 
			super.onResume();
			String action =  getIntent().getAction();
			Log.d("onResume", "onResume:"+action);
			if(kisPayment != null && !kisPayment.getSerial().isConnected()) {
				if( !kisPayment.getSerial().enumerate() ) {
					Toast.makeText(this, "no more devices found", Toast.LENGTH_SHORT).show();     
					return;
				} else {
					Log.d("onResume", "onResume:enumerate succeeded!");
				}    		 
				Toast.makeText(this, "attached", Toast.LENGTH_SHORT).show();
			}//if isConnected  
			Log.d("onResume", "Leave onResume"); 
		}    

		@Override
		protected void onDestroy() {
			Log.d("onDestroy", "Enter onDestroy");   
			if(kisPayment.getSerial()!=null) {
				kisPayment.getSerial().end();
				kisPayment.setSerial(null);
				//kisPayment.stopAndInitPayment();
			}    
			super.onDestroy();        
			Log.d("onDestroy", "Leave onDestroy");
		}
	 	
	 	
	 	
	 	//Receive 등록
		public void PaymentProcess() {
			
			//영수증 프린
		//	kisPayment.setLogoImagePath(Environment.getExternalStorageDirectory().getPath()+"/logo.bmp");
					
			kisPayment.setOnResultReceived(new OnKisResultReceived() {
				@SuppressWarnings("unused")
				@Override
				public void returnReceivedData(KisResultOptions resultOptions) throws JSONException {
					
					//결과 값
					System.out.println("resultOptions====>"+resultOptions);
					
					retry(resultOptions.toJSONString());
					
					if(resultOptions != null){
						
					
							
					}else {
						
						
						
						
					}
						
				}
			});
					
		}

		public void retry(final String txt){
			
		
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
				
					tv_test.setText(txt);
				}
			});
			

		}
}

package jeonbuk.open.os;

public class ResultOptions extends Options {
	/* 단말기 번호 */
	private String terminalNo;
	/* 부가세 */
	private String approvalSurtax;
	/* 봉사료 */
	private String approvalServiceCharge;
	/* 거래일자 Yymmddhhmmss 12자리 */
	private String approvalDy;
	/* 원거래일자/세금 yyyymmdd  */
	private String approvalDateOrg;
	/* 원거래승인번호/봉사료 (신용8자리, 직불 12자리, 거절시 공란) */
	private String approvalNoOrg;
	/* 매입사코드 */
	private String purchaseCode;
	/* 매입카드사명 */
	private String purchaseName;
	/* 발급사코드 */
	private String issuebinCode;
	/* 발급카드사명 */
	private String issuebinName;
	/* 가맹점번호 */
	private String approvalShop;
	/* 거래일련번호 */
	private String dealNo;
	/* 포인트 카드정보 */
	private String pointCardInfo;
	/* 포인트 승인번호 */
	private String pointApprovalNo;
	/* 포인트 발급사코드 */
	private String pointIssueBinCode;
	/* 포인트 발급사명 */
	private String pointIssueBinName;
	/* 포인트 가맹점번호 */
	private String pointShopNo;
	/* 할인 후 금액 */
	private String pointDcAfterAmount;
	/* 발생 포인트 */
	private String pointOccur;
	/* 가용 포인트 */
	private String pointUsePossible;
	/* 누적포인트  (130430일 현재 누적포인트는 정보가 아예 안오는것으로 확인)*/
	private String pointAccumulate;
	/* 거래시간 */
	private String approvalTime;
	
	public String getTerminalNo() {
		return terminalNo;
	}
	public void setTerminalNo(String terminalNo) {
		this.terminalNo = terminalNo;
	}
	public String getApprovalSurtax() {
		return approvalSurtax;
	}
	public void setApprovalSurtax(String approvalSurtax) {
		this.approvalSurtax = approvalSurtax;
	}
	public String getApprovalServiceCharge() {
		return approvalServiceCharge;
	}
	public void setApprovalServiceCharge(String approvalServiceCharge) {
		this.approvalServiceCharge = approvalServiceCharge;
	}
	public String getApprovalDy() {
		return approvalDy;
	}
	public void setApprovalDy(String approvalDy) {
		this.approvalDy = approvalDy;
	}
	public String getApprovalDateOrg() {
		return approvalDateOrg;
	}
	public void setApprovalDateOrg(String approvalDateOrg) {
		this.approvalDateOrg = approvalDateOrg;
	}
	public String getApprovalNoOrg() {
		return approvalNoOrg;
	}
	public void setApprovalNoOrg(String approvalNoOrg) {
		this.approvalNoOrg = approvalNoOrg;
	}
	public String getPurchaseCode() {
		return purchaseCode;
	}
	public void setPurchaseCode(String purchaseCode) {
		this.purchaseCode = purchaseCode;
	}
	public String getPurchaseName() {
		return purchaseName;
	}
	public void setPurchaseName(String purchaseName) {
		this.purchaseName = purchaseName;
	}
	public String getIssuebinCode() {
		return issuebinCode;
	}
	public void setIssuebinCode(String issuebinCode) {
		this.issuebinCode = issuebinCode;
	}
	public String getIssuebinName() {
		return issuebinName;
	}
	public void setIssuebinName(String issuebinName) {
		this.issuebinName = issuebinName;
	}
	public String getApprovalShop() {
		return approvalShop;
	}
	public void setApprovalShop(String approvalShop) {
		this.approvalShop = approvalShop;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getPointCardInfo() {
		return pointCardInfo;
	}
	public void setPointCardInfo(String pointCardInfo) {
		this.pointCardInfo = pointCardInfo;
	}
	public String getPointApprovalNo() {
		return pointApprovalNo;
	}
	public void setPointApprovalNo(String pointApprovalNo) {
		this.pointApprovalNo = pointApprovalNo;
	}
	public String getPointIssueBinCode() {
		return pointIssueBinCode;
	}
	public void setPointIssueBinCode(String pointIssueBinCode) {
		this.pointIssueBinCode = pointIssueBinCode;
	}
	public String getPointShopNo() {
		return pointShopNo;
	}
	public void setPointShopNo(String pointShopNo) {
		this.pointShopNo = pointShopNo;
	}
	public String getPointDcAfterAmount() {
		return pointDcAfterAmount;
	}
	public void setPointDcAfterAmount(String pointDcAfterAmount) {
		this.pointDcAfterAmount = pointDcAfterAmount;
	}
	public String getPointOccur() {
		return pointOccur;
	}
	public void setPointOccur(String pointOccur) {
		this.pointOccur = pointOccur;
	}
	public String getPointUsePossible() {
		return pointUsePossible;
	}
	public void setPointUsePossible(String pointUsePossible) {
		this.pointUsePossible = pointUsePossible;
	}
	public String getPointAccumulate() {
		return pointAccumulate;
	}
	public void setPointAccumulate(String pointAccumulate) {
		this.pointAccumulate = pointAccumulate;
	}
	public String getPointIssueBinName() {
		return pointIssueBinName;
	}
	public void setPointIssueBinName(String pointIssueBinName) {
		this.pointIssueBinName = pointIssueBinName;
	}
	public String getApprovalTime() {
		return approvalTime;
	}
	public void setApprovalTime(String approvalTime) {
		this.approvalTime = approvalTime;
	}
	
	
}

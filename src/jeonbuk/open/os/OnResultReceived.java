package jeonbuk.open.os;

import org.json.JSONException;

public interface OnResultReceived {
	void returnReceivedData(ResultOptions resultOptions) throws JSONException;
}

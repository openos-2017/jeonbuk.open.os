package jeonbuk.open.os;

public class KisOptions {
	/* 합계금액 */
	private String paymentAmount = "";
	/* 거래종류코드 */
	private String serviceCode = "";
	/* 할부개월 (현금영수증일떄는 (01법인, 02개인)) */
	private String approvalPaymonth = "";
	/* wcc (swipe:A, keyin:@) */
	private String wcc = "";
	/* 포인트카드에대한 wcc */
	private String wccPointCard = "";
	/* track-2(카드번호+'='+유효기간) */
	private String approvalCard = "";
	/* 거래일자 */
	private String approvalDate = "";
	/* 승인번호 */
	private String approvalNo = "";
	/* 포인트카드 정보 track2 */
	private String approvalCardPointcard = "";
	/* 포인트 취소시 원승인 번호 */
	private String approvalDatePointCard = "";
	
	/* 거래 유형 */
	private String paymentType = "";
	/* 마스크 정보 */
	private String maskMessage = "";
	
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getApprovalPaymonth() {
		return approvalPaymonth;
	}
	public void setApprovalPaymonth(String approvalPaymonth) {
		this.approvalPaymonth = approvalPaymonth;
	}
	public String getWcc() {
		return wcc;
	}
	public void setWcc(String wcc) {
		this.wcc = wcc;
	}
	public String getWccPointCard() {
		return wccPointCard;
	}
	public void setWccPointCard(String wccPointCard) {
		this.wccPointCard = wccPointCard;
	}
	public String getApprovalCard() {
		return approvalCard;
	}
	public void setApprovalCard(String approvalCard) {
		this.approvalCard = approvalCard;
	}
	public String getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}
	public String getApprovalNo() {
		return approvalNo;
	}
	public void setApprovalNo(String approvalNo) {
		this.approvalNo = approvalNo;
	}
	public String getApprovalCardPointcard() {
		return approvalCardPointcard;
	}
	public void setApprovalCardPointcard(String approvalCardPointcard) {
		this.approvalCardPointcard = approvalCardPointcard;
	}
	public String getApprovalDatePointCard() {
		return approvalDatePointCard;
	}
	public void setApprovalDatePointCard(String approvalDatePointCard) {
		this.approvalDatePointCard = approvalDatePointCard;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getMaskMessage() {
		return maskMessage;
	}
	public void setMaskMessage(String maskMessage) {
		this.maskMessage = maskMessage;
	}
	public String toJSONString() {
		return "{ paymentAmount\" : \"" + paymentAmount + "\", \"serviceCode\" : \"" + serviceCode + "\", \"approvalPaymonth="
				+ approvalPaymonth + "\", \"wcc\" : \"" + wcc + "\", \"wccPointCard\" : \"" + wccPointCard + "\", \"approvalCard="
				+ approvalCard + "\", \"approvalDate\" : \"" + approvalDate + "\", \"approvalNo\" : \"" + approvalNo
				+ "\", \"approvalCardPointcard\" : \"" + approvalCardPointcard + "\", \"approvalDatePointCard="
				+ approvalDatePointCard + "\", \"paymentType\" : \"" + paymentType + "\", \"maskMessage\" : \"" + maskMessage + "}";
	}
	
	
}
